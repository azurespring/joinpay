<?php
require __DIR__ . '/../vendor/autoload.php';

use AzureSpring\Joinpay\Client;
use AzureSpring\Joinpay\Model\PaymentOptions;
use AzureSpring\Joinpay\Model\TransferOptions;
use GuzzleHttp\Client as Guzzle;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Logger;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Uid\Uuid;

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__ . '/.env');

$app = \DI\Bridge\Slim\Bridge::create();

$logger = new Monolog\Logger('joinpay');
$logger->pushHandler(new Monolog\Handler\ErrorLogHandler());
$app->getContainer()->set(LoggerInterface::class, $logger);

$guzzle = new Guzzle(['base_uri' => Client::BASE_URI]);
$psr17Factory = new Psr17Factory();
$client = new Client(
    $_SERVER['JOINPAY_MERCHANT_NO'],
    $_SERVER['JOINPAY_ALT_MERCHANT_NO'],
    $_SERVER['JOINPAY_SECRET'],
    $guzzle,
    $psr17Factory,
    $logger
);
$app->getContainer()->set(Client::class, $client);

$app->post('/payments', function (Request $request, Response $response, Client $client) {
    $body = $request->getParsedBody();
    $opts = new PaymentOptions(
        Uuid::v1()->toBase58(),
        [PaymentOptions::TYPE_WX_APPX, PaymentOptions::TYPE_WX_MP][$body['type']],
        $body['title'],
        $body['app_id'],
        $body['total'],
        $request->getUri()->withPath('/payments/_ack')
    );
    if (in_array($opts->getType(), [PaymentOptions::TYPE_WX_MP])) {
        $opts->setOpenId($body['open_id']);
    }

    $payment = $client->createPayment($opts);
    $response->getBody()->write(json_encode($payment->getProvision()));

    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/payments/_ack', function (Request $request, Response $response, Client $client) {
    $tx = $client->acceptPayment($request);

    return $client->acknowledge();
});

$app->post('/transfers', function (Request $request, Response $response, Client $client) {
    $body = $request->getParsedBody();
    $transfer = new TransferOptions(
        Uuid::v1()->toBase58(),
        TransferOptions::TYPE_DAILY,
        TransferOptions::USAGE_SERVICE,
        @$body['synopsis'] ?? '',
        $body['account_number'],
        $body['account_name'],
        @$body['cnaps_code'],
        $body['total']
    );

    $client->createTransfer(
        $transfer
            ->setApproved(!!@$body['force'])
            ->setNotifyUrl((string) $request->getUri()->withPath('/transfers/_ack'))
    );
    $response->getBody()->write($transfer->getNotifyUrl());

    return $response;
});

$app->post('/transfers/_ack', function (Request $request, Response $response, Client $client) {
    $transfer = $client->acceptTransfer($request);
    $response->getBody()->write($transfer->getPermanentId());

    return $response;
});

$app->run();
