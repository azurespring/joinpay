<?php

namespace AzureSpring\Joinpay;

use AzureSpring\Joinpay\Exception\ServiceException;
use AzureSpring\Joinpay\Exception\SignatureException;
use AzureSpring\Joinpay\Model\Balance;
use AzureSpring\Joinpay\Model\BatchOptions;
use AzureSpring\Joinpay\Model\Payment;
use AzureSpring\Joinpay\Model\PaymentOptions;
use AzureSpring\Joinpay\Model\Refund;
use AzureSpring\Joinpay\Model\RefundOptions;
use AzureSpring\Joinpay\Model\Repayment;
use AzureSpring\Joinpay\Model\Transaction;
use AzureSpring\Joinpay\Model\Transfer;
use AzureSpring\Joinpay\Model\TransferOptions;
use GuzzleHttp\Client as Guzzle;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use function GuzzleHttp\Psr7\parse_query;

class Client
{
    const BASE_URI = 'https://www.joinpay.com';
    const STATUS_OK = '100';

    private $merchantNo;

    private $altMerchantNo;

    private $secret;

    private $guzzle;

    private $responseFactory;

    private $logger;

    public function __construct(string $merchantNo, string $altMerchantNo, string $secret, Guzzle $guzzle, ResponseFactoryInterface $responseFactory, LoggerInterface $logger)
    {
        $this->merchantNo = $merchantNo;
        $this->altMerchantNo = $altMerchantNo;
        $this->secret = $secret;
        $this->guzzle = $guzzle;
        $this->responseFactory = $responseFactory;
        $this->logger = $logger;
    }

    public function createPayment(PaymentOptions $options): Payment
    {
        $result = $this->request('/trade/uniPayApi.action', $options->eject() + [
            'p1_MerchantNo' => $this->merchantNo,
            'qa_TradeMerchantNo' => $this->altMerchantNo,
        ]);
        $this->assertEquals($options->getPermanentId(), $result['OrderNo'], 'Order No. does not match');
        $this->assertEquals(number_format($options->getTotal() / 1e2, 2), $result['Amount'], 'Amount does not match');

        switch ($result['FrpCode']) {
            case PaymentOptions::TYPE_WX_APPX:
            case PaymentOptions::TYPE_WX_MP:
                return new Payment($result['TrxNo'], \GuzzleHttp\json_decode($result['Result'], true));

            default:
                throw new \LogicException();
        }
    }

    public function acceptPayment(RequestInterface $request): Transaction
    {
        $result = $this->check(parse_query($request->getUri()->getQuery()), ['ra_PayTime', 'rb_DealTime']);
        $this->assertEquals($this->merchantNo, $result['MerchantNo'], 'Merchant No. does not match');

        return new Transaction(
            $result['OrderNo'],
            $result['TrxNo'],
            (int) ($result['Amount'] * 1e2),
            (string) $result['Status'],
            \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $result['PayTime'], new \DateTimeZone('Asia/Shanghai'))
        );
    }

    public function findPayment(string $permanentId): Transaction
    {
        $result = $this->request('/trade/queryOrder.action', [
            'p1_MerchantNo' => $this->merchantNo,
            'p2_OrderNo' => $permanentId,
        ]);
        $this->assertEquals($permanentId, $result['OrderNo'], 'Order No. does not match');

        return new Transaction(
            $result['OrderNo'],
            $result['TrxNo'],
            (int) ($result['Amount'] * 1e2),
            (string) $result['Status']
        );
    }

    public function createRefund(RefundOptions $options): Refund
    {
        $result = $this->request('/trade/refund.action', $options->eject() + [
            'p1_MerchantNo' => $this->merchantNo,
        ]);
        $this->assertEquals($this->merchantNo, $result['MerchantNo'], 'Merchant No. does not match');
        $this->assertEquals($options->getPaymentId(), $result['OrderNo'], 'Order No. does not match');

        return new Refund(
            $result['RefundOrderNo'],
            $result['RefundTrxNo'],
            (int) ($result['RefundAmount'] * 1e2),
            (string) $result['Status']
        );
    }

    public function acceptRefund(RequestInterface $request): Repayment
    {
        $result = $this->check(parse_query($request->getUri()->getQuery()));
        $this->assertEquals($this->merchantNo, $result['MerchantNo'], 'Merchant No. does not match');

        return new Repayment(
            $result['RefundOrderNo'],
            $result['RefundTrxNo'],
            (int) ($result['RefundAmount'] * 1e2),
            (string) $result['Status']
        );
    }

    public function findRefund(string $permanentId): Repayment
    {
        $result = $this->request('/trade/queryRefund.action', [
            'p1_MerchantNo' => $this->merchantNo,
            'p2_RefundOrderNo' => $permanentId,
            'p3_Version' => RefundOptions::VERSION,
        ]);
        $this->assertEquals($permanentId, $result['RefundOrderNo'], 'Refund order No. does not match');

        return new Repayment(
            $result['RefundOrderNo'],
            $result['RefundTrxNo'],
            (int) ($result['RefundAmount'] * 1e2),
            (string) $result['Status']
        );
    }

    public function createTransfer(TransferOptions $options)
    {
        $result = $this->request2('/payment/pay/singlePay', ['userNo' => $this->merchantNo] + $options->eject(), [
            'errorCode',
            'errorDesc',
            'userNo',
            'merchantOrderNo',
        ]);
        $this->assertEquals($options->getPermanentId(), $result['merchantOrderNo'], 'Transfer order No. does not match');
    }

    public function acceptTransfer(RequestInterface $request): Transfer
    {
        $result = $this->check2(\GuzzleHttp\json_decode($request->getBody()->getContents(), true), [
            'status',
            'errorCode',
            'errorCodeDesc',
            'userNo',
            'merchantOrderNo',
            'platformSerialNo',
            'receiverAccountNoEnc',
            'receiverNameEnc',
            'paidAmount',
            'fee',
        ]);

        return new Transfer(
            $result['merchantOrderNo'],
            $result['platformSerialNo'],
            $result['receiverAccountNoEnc'],
            $result['receiverNameEnc'],
            (int) ($result['paidAmount'] * 1e2),
            (int) ($result['fee'] * 1e2),
            $result['status']
        );
    }

    public function findTransfer(string $permanentId)
    {
        $result = $this->request2('/payment/pay/singlePayQuery', ['userNo' => $this->merchantNo, 'merchantOrderNo' => $permanentId], [
            'status',
            'errorCode',
            'errorDesc',
            'userNo',
            'merchantOrderNo',
            'platformSerialNo',
            'receiverAccountNoEnc',
            'receiverNameEnc',
            'paidAmount',
            'fee',
        ]);
        $this->assertEquals($permanentId, $result['merchantOrderNo'], 'Transfer order No. does not match');
        if ($result['errorCode']) {
            throw new ServiceException($result['errorDesc'], $result['errorCode']);
        }

        return new Transfer(
            $result['merchantOrderNo'],
            $result['platformSerialNo'],
            $result['receiverAccountNoEnc'],
            $result['receiverNameEnc'],
            (int) ($result['paidAmount'] * 1e2),
            (int) ($result['fee'] * 1e2),
            $result['status']
        );
    }

    public function createBatch(BatchOptions $options)
    {
    }

    public function acceptBatch(RequestInterface $request)
    {
    }

    public function findBatch(string $permanentId)
    {
    }

    public function getBalance(): Balance
    {
        $result = $this->request2('/payment/pay/accountBalanceQuery', ['userNo' => $this->merchantNo], [
            'userNo',
            'userName',
            'currency',
            'useAbleSettAmount',
            'availableSettAmountFrozen',
            'errorCode',
            'errorDesc',
        ]);

        return new Balance(
            (int) ($result['useAbleSettAmount'] * 1e2),
            (int) ($result['availableSettAmountFrozen'] * 1e2)
        );
    }

    public function acknowledge(): ResponseInterface
    {
        $response = $this->responseFactory->createResponse();
        $response->getBody()->write('success');

        return $response->withHeader('Content-type', 'text/plain; charset="utf-8"');
    }

    protected function request(string $uri, array $parameters): array
    {
        $this->logger->debug('request()', ['uri' => $uri, 'parameters' => $parameters]);
        $response = $this->guzzle->request('POST', $uri, [
            'form_params' => $parameters + [
                'hmac' => $this->sign($parameters),
            ],
        ]);
        $result = $this->check(\GuzzleHttp\json_decode($response->getBody()->getContents(), true));
        if (self::STATUS_OK !== (string) $result['Code']) {
            throw new ServiceException($result['CodeMsg'], $result['Code']);
        }

        /* $this->assertEquals(self::VERSION, $result['Version'], 'Version does not match'); */
        $this->assertEquals($this->merchantNo, $result['MerchantNo'], 'Merchant No. does not match');

        return $result;
    }

    protected function request2(string $uri, array $parameters, array $keys): array
    {
        $this->logger->debug('request2()', ['uri' => $uri, 'parameters' => $parameters]);
        $response = $this->guzzle->request('POST', $uri, [
            'json' => array_filter($parameters + [
                'hmac' => $this->sign($parameters, true),
            ]),
        ]);
        $result = $this->check2(\GuzzleHttp\json_decode($response->getBody()->getContents(), true), $keys);
        if ('2001' !== (string) $result['statusCode']) {
            throw new ServiceException($result['message'], $result['statusCode']);
        }

        $this->assertEquals($this->merchantNo, $result['data']['userNo'], 'Merchant No. does not match');

        return $result['data'];
    }

    protected function check(array $result, array $urldecode = []): array
    {
        $this->logger->debug('check()', ['result' => $result]);
        $hmac = strtolower($result['hmac']);
        unset($result['hmac']);
        foreach ($result as $k => &$v) {
            if (in_array($k, $urldecode)) {
                $v = urldecode($v);
            } elseif ($this->isMoney($k)) {
                $v = number_format($v, 2);
            }
        }
        if ($hmac !== $this->sign($result)) {
            throw new SignatureException();
        }

        return array_combine(
            array_map(
                function ($k) {
                    return preg_replace('/r._([^_]*)(_.*)?/', '$1', $k);
                },
                array_keys($result)
            ),
            $result
        );
    }

    protected function check2(array $result, array $keys): array
    {
        $this->logger->debug('check2()', ['result' => $result]);
        $parameters = [];
        $signature = '';
        if (array_key_exists('data', $result)) {
            $signature = $result['data']['hmac'];

            $parameters = [$result['statusCode'], $result['message']];
            foreach ($keys as $k) {
                $v = @$result['data'][$k];
                if ($this->isMoney($k)) {
                    $v = number_format($v, 2);
                }

                $parameters[] = $v;
            }
        } else {
            $signature = $result['hmac'];

            foreach ($keys as $k) {
                $v = @$result[$k];
                if ($this->isMoney($k)) {
                    $v = number_format($v, 2);
                }

                $parameters[] = $v;
            }
        }
        if ($signature !== $this->sign($parameters, true)) {
            throw new SignatureException();
        }

        return $result;
    }

    protected function isMoney($key)
    {
        return preg_match('/\b(amount|fee)\b/', strtolower(preg_replace('/([A-Z])/', '-$1', $key)));
    }

    protected function sign(array $parameters, $sorted = false): string
    {
        if (!$sorted) {
            ksort($parameters);
        }

        return md5(implode($parameters).$this->secret);
    }

    protected function assertEquals($expected, $actual, string $message)
    {
        if ((string) $expected !== $actual) {
            throw new \UnexpectedValueException($message);
        }
    }
}
