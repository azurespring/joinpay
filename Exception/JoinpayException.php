<?php

namespace AzureSpring\Joinpay\Exception;

interface JoinpayException extends \Throwable
{
}
