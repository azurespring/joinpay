<?php

namespace AzureSpring\Joinpay\Exception;

class SignatureException extends \RuntimeException implements JoinpayException
{
}
