<?php

namespace AzureSpring\Joinpay\Exception;

class ServiceException extends \RuntimeException implements JoinpayException
{
}
