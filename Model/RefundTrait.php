<?php

namespace AzureSpring\Joinpay\Model;

trait RefundTrait
{
    /** @var string */
    private $permanentId;

    /** @var string */
    private $referenceNo;

    /** @var int */
    private $total;

    /** @var string */
    private $status;

    public function __construct(string $permanentId, string $referenceNo, int $total, string $status)
    {
        $this->permanentId = $permanentId;
        $this->referenceNo = $referenceNo;
        $this->total = $total;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @return string
     */
    public function getReferenceNo(): string
    {
        return $this->referenceNo;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
}
