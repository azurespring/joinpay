<?php

namespace AzureSpring\Joinpay\Model;

class PaymentOptions
{
    const VERSION = '1.0';

    const TYPE_WX_APPX = 'WEIXIN_APP3';
    const TYPE_WX_MP = 'WEIXIN_XCX';

    const CNY = '1';

    /** @var string */
    private $permanentId;

    /** @var string */
    private $type;

    /** @var string */
    private $title;

    /** @var string|null */
    private $synopsis;

    /** @var string */
    private $appId;

    /** @var string|null */
    private $openId;

    /** @var int */
    private $total;

    /** @var string */
    private $notifyUrl;

    public function __construct(string $permanentId, string $type, string $title, string $appId, int $total, string $notifyUrl)
    {
        $this->permanentId = $permanentId;
        $this->type = $type;
        $this->title = $title;
        $this->appId = $appId;
        $this->total = $total;
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @param string $permanentId
     *
     * @return $this
     */
    public function setPermanentId(string $permanentId): self
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    /**
     * @param string|null $synopsis
     *
     * @return $this
     */
    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     *
     * @return $this
     */
    public function setAppId(string $appId): self
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOpenId(): ?string
    {
        return $this->openId;
    }

    /**
     * @param string|null $openId
     *
     * @return $this
     */
    public function setOpenId(?string $openId): self
    {
        $this->openId = $openId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string $notifyUrl
     *
     * @return $this
     */
    public function setNotifyUrl(string $notifyUrl): self
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    public function eject(): array
    {
        return [
            'p0_Version' => self::VERSION,
            'p2_OrderNo' => $this->getPermanentId(),
            'p3_Amount' => number_format($this->getTotal() / 1e2, 2),
            'p4_Cur' => self::CNY,
            'p5_ProductName' => $this->getTitle(),
            'p6_ProductDesc' => $this->getSynopsis(),
            'p9_NotifyUrl' => $this->getNotifyUrl(),
            'q1_FrpCode' => $this->getType(),
            'q5_OpenId' => $this->getOpenId(),
            'q7_AppId' => $this->getAppId(),
        ];
    }
}
