<?php

namespace AzureSpring\Joinpay\Model;

class Transfer
{
    const FAILURE = '204';
    const SUCCESS = '205';

    /** @var string */
    private $permanentId;

    /** @var string */
    private $referenceNo;

    /** @var string */
    private $accountNumber;

    /** @var string */
    private $accountName;

    /** @var int */
    private $total;

    /** @var int */
    private $fee;

    /** @var string */
    private $status;

    public function __construct(string $permanentId, string $referenceNo, string $accountNumber, string $accountName, int $total, int $fee, string $status)
    {
        $this->permanentId = $permanentId;
        $this->referenceNo = $referenceNo;
        $this->accountNumber = $accountNumber;
        $this->accountName = $accountName;
        $this->total = $total;
        $this->fee = $fee;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @return string
     */
    public function getReferenceNo(): string
    {
        return $this->referenceNo;
    }

    /**
     * @return string
     */
    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    /**
     * @return string
     */
    public function getAccountName(): string
    {
        return $this->accountName;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getFee(): int
    {
        return $this->fee;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
}
