<?php

namespace AzureSpring\Joinpay\Model;

class TransferOptionsLine
{
    const PERSONAL = '201';
    const BUSINESS = '204';

    const CNY = '201';

    const USAGE_GOODS = '204';
    const USAGE_SERVICE = '205';

    const UNAPPROVED = '201';
    const APPROVED = '202';

    /** @var string */
    protected $permanentId;

    /** @var string */
    protected $usage;

    /** @var string */
    protected $synopsis;

    /** @var string */
    protected $accountNumber;

    /** @var string */
    protected $accountName;

    /** @var string|null */
    protected $cnapsCode;

    /** @var int */
    protected $total;

    /** @var bool */
    protected $approved;

    public function __construct(string $permanentId, string $usage, string $synopsis, string $accountNumber, string $accountName, ?string $cnapsCode, int $total)
    {
        $this->permanentId = $permanentId;
        $this->usage = $usage;
        $this->synopsis = $synopsis;
        $this->accountName = $accountName;
        $this->accountNumber = $accountNumber;
        $this->cnapsCode = $cnapsCode;
        $this->total = $total;
        $this->approved = false;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @param string $permanentId
     *
     * @return $this
     */
    public function setPermanentId(string $permanentId): self
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsage(): string
    {
        return $this->usage;
    }

    /**
     * @param string $usage
     *
     * @return $this
     */
    public function setUsage(string $usage): self
    {
        $this->usage = $usage;

        return $this;
    }

    /**
     * @return string
     */
    public function getSynopsis(): string
    {
        return $this->synopsis;
    }

    /**
     * @param string $synopsis
     *
     * @return $this
     */
    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     *
     * @return $this
     */
    public function setAccountNumber(string $accountNumber): self
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountName(): string
    {
        return $this->accountName;
    }

    /**
     * @param string $accountName
     *
     * @return $this
     */
    public function setAccountName(string $accountName): self
    {
        $this->accountName = $accountName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCnapsCode(): ?string
    {
        return $this->cnapsCode;
    }

    /**
     * @param string|null $cnapsCode
     *
     * @return $this
     */
    public function setCnapsCode(?string $cnapsCode): self
    {
        $this->cnapsCode = $cnapsCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     *
     * @return $this
     */
    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    public function eject(): array
    {
        return [
            'merchantOrderNo' => $this->getPermanentId(),
            'receiverAccountNoEnc' => $this->getAccountNumber(),
            'receiverNameEnc' => $this->getAccountName(),
            'receiverAccountType' => $this->getCnapsCode() ? self::BUSINESS : self::PERSONAL,
            'receiverBankChannelNo' => $this->getCnapsCode(),
            'paidAmount' => number_format($this->getTotal() / 1e2, 2),
            'currency' => self::CNY,
            'isChecked' => $this->approved ? self::APPROVED : self::UNAPPROVED,
            'paidDesc' => $this->getSynopsis(),
            'paidUse' => $this->getUsage(),
        ];
    }
}
