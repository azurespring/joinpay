<?php

namespace AzureSpring\Joinpay\Model;

class Payment
{
    /** @var string */
    private $referenceNo;

    /** @var mixed */
    private $provision;

    public function __construct(string $referenceNo, $provision)
    {
        $this->referenceNo = $referenceNo;
        $this->provision = $provision;
    }

    /**
     * @return string
     */
    public function getReferenceNo(): string
    {
        return $this->referenceNo;
    }

    /**
     * @return mixed
     */
    public function getProvision()
    {
        return $this->provision;
    }
}
