<?php

namespace AzureSpring\Joinpay\Model;

class RefundStatus
{
    const STATUS_OK = '100';
    const STATUS_ERROR = '101';
    const STATUS_IN_PROCESS = '100';
}
