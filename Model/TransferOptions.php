<?php

namespace AzureSpring\Joinpay\Model;

class TransferOptions extends TransferOptionsLine
{
    const TYPE_ORDINARY = 'BANK_PAY_ORDINARY_ORDER';
    const TYPE_DAILY = 'BANK_PAY_DAILY_ORDER';

    /** @var string */
    private $type;

    /** @var string|null */
    private $notifyUrl;

    /** @var \DateTimeImmutable */
    private $createdAt;

    public function __construct(string $permanentId, string $type, string $usage, string $synopsis, string $accountNumber, string $accountName, ?string $cnapsCode, int $total, ?\DateTimeImmutable $createdAt = null)
    {
        parent::__construct($permanentId, $usage, $synopsis, $accountNumber, $accountName, $cnapsCode, $total);

        $this->type = $type;
        $this->createdAt = $createdAt ?? new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotifyUrl(): ?string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string|null $notifyUrl
     *
     * @return $this
     */
    public function setNotifyUrl(?string $notifyUrl): self
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeImmutable $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function eject(): array
    {
        return (
            [
                'productCode' => $this->getType(),
                'requestTime' => $this->getCreatedAt()->setTimezone(new \DateTimeZone('Asia/Shanghai'))->format('Y-m-d H:i:s'),
            ] +
            parent::eject() +
            [
                'callbackUrl' => $this->getNotifyUrl(),
            ]
        );
    }
}
