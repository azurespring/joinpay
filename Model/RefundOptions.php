<?php

namespace AzureSpring\Joinpay\Model;

class RefundOptions
{
    const VERSION = '2.0';

    /** @var string */
    private $permanentId;

    /** @var string */
    private $paymentId;

    /** @var string|null */
    private $reason;

    /** @var int */
    private $total;

    /** @var string */
    private $notifyUrl;

    public function __construct(string $permanentId, string $paymentId, int $total, string $notifyUrl)
    {
        $this->permanentId = $permanentId;
        $this->paymentId = $paymentId;
        $this->total = $total;
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @param string $permanentId
     *
     * @return $this
     */
    public function setPermanentId(string $permanentId): self
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @param string $paymentId
     *
     * @return $this
     */
    public function setPaymentId(string $paymentId): self
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * @param string|null $reason
     *
     * @return $this
     */
    public function setReason(?string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string $notifyUrl
     *
     * @return $this
     */
    public function setNotifyUrl(string $notifyUrl): self
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    public function eject(): array
    {
        return [
            'p2_OrderNo' => $this->paymentId,
            'p3_RefundOrderNo' => $this->permanentId,
            'p4_RefundAmount' => number_format($this->getTotal() / 1e2, 2),
            'p5_RefundReason' => $this->reason,
            'p6_NotifyUrl' => $this->getNotifyUrl(),
            'q1_version' => self::VERSION,
        ];
    }
}
