<?php

namespace AzureSpring\Joinpay\Model;

class BatchOptions
{
    /** @var string */
    private $permanentId;

    /** @var string */
    private $type;

    /** @var string|null */
    private $notifyUrl;

    /** @var \DateTimeImmutable */
    private $createdAt;

    /** @var TransferOptionsLine[] */
    private $lines;

    public function __construct(string $permanentId, string $type, ?\DateTimeImmutable $createdAt = null, array $lines = [])
    {
        $this->permanentId = $permanentId;
        $this->type = $type;
        $this->createdAt = $createdAt ?? new \DateTimeImmutable();
        $this->lines = $lines;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @param string $permanentId
     *
     * @return $this
     */
    public function setPermanentId(string $permanentId): self
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotifyUrl(): ?string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string|null $notifyUrl
     *
     * @return $this
     */
    public function setNotifyUrl(?string $notifyUrl): self
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeImmutable $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return TransferOptionsLine[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    /**
     * @param TransferOptionsLine $line
     *
     * @return $this
     */
    public function addLine(TransferOptionsLine $line): self
    {
        $this->lines[] = $line;

        return $this;
    }

    public function eject(): array
    {
        return [
            'productCode' => $this->getType(),
            'merchantBatchNo' => $this->getPermanentId(),
            'requestCount' => count($this->getLines()),
            'requestAmount' => number_format(0, 2),
            'requestTime' => $this->getCreatedAt()->setTimezone(new \DateTimeZone('Asia/Shanghai'))->format('Y-m-d H:i:s'),
            'callbackUrl' => $this->getNotifyUrl(),
            'details' => array_map(
                function (TransferOptionsLine $line) {
                    return $line->eject();
                },
                $this->getLines()
            ),
        ];
    }
}
