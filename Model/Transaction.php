<?php

namespace AzureSpring\Joinpay\Model;

class Transaction
{
    const STATUS_OK = '100';
    const STATUS_ERROR = '101';
    const STATUS_CREATED = '102';
    const STATUS_CANCELLED = '103';

    /** @var string */
    private $permanentId;

    /** @var string */
    private $referenceNo;

    /** @var int */
    private $total;

    /** @var string */
    private $status;

    /** @var \DateTimeImmutable|null */
    private $closedAt;

    public function __construct(string $permanentId, string $referenceNo, int $total, string $status, ?\DateTimeImmutable $closedAt = null)
    {
        $this->permanentId = $permanentId;
        $this->referenceNo = $referenceNo;
        $this->total = $total;
        $this->status = $status;
        $this->closedAt = $closedAt;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @return string
     */
    public function getReferenceNo(): string
    {
        return $this->referenceNo;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getClosedAt(): ?\DateTimeImmutable
    {
        return $this->closedAt;
    }
}
