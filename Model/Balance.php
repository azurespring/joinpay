<?php

namespace AzureSpring\Joinpay\Model;

class Balance
{
    /** @var int */
    private $available;

    /** @var int */
    private $reserved;

    public function __construct(int $available, int $reserved)
    {
        $this->available = $available;
        $this->reserved = $reserved;
    }

    /**
     * @return int
     */
    public function getAvailable(): int
    {
        return $this->available;
    }

    /**
     * @return int
     */
    public function getReserved(): int
    {
        return $this->reserved;
    }
}
